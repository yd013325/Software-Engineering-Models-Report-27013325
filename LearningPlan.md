# **Project plan - individual coursework**

### **Table of Contants**
1. Abstract
2. Background
3. Introducion
4. Reflections
5. References

### **Abstract**

Computer science is the discipline that systematically studies the theoretical basis of information and computing, and how they are implemented and applied in computer systems. The major in computer science are accepted and recognized by an increasing number of people. The computer field is very attractive and people can explore the world through this field. The famous computer scientist Edsger Dijkstra once pointed out: "Computer science is no more about computers than astronomy is about telescopes."

The subject of computer science has a wide range of knowledge and is full of challenges in learning. Students want to learn this major, a wonderful study plan is especially important. The most important feature of a good study plan is its distinct purpose. The plan serves the purpose, the plan arises for the purpose, exists for the purpose, and is formulated for the purpose. Planning is the concreteization of goals, a rational bridge connecting reality and goals, and everything planned is for purpose.

### **Background**

Today, with emphasis on executive power, everyone seems willing to see a busy scene. In fact, behind the busy, we should seriously think about what we are busy with? Why are you busy? Should you be busy?

In the 19th century Italian economics Pareto proposed the famous Pareto principle. The core content is that 80% of the results in life are almost exclusively due to 20% of activities. He told us that the vast majority of busyness is ineffective and this theory has been widely validated.

Busy stems from our becoming a slave to the “emergency” and dealing with the so-called urgent things that are endless all day long. Most of the urgency is usually not important, because our work has no clear goals and plans, so our actions are plagued by urgency.

In today's increasingly busy society, no matter what, there is a plan that is good. Students are no exception, and studying at the university is to provide more choices for the future. In order to achieve this goal, it is especially important to develop a study plan.

### **Introducion**

How to develop a three-year study plan? My plan is to set a goal first. Goals have a directive effect on the plan, and any plan is derived from the goal. And my goal is not only the final degree after three years, but also to learn solid skills in these three years. After three years, whether it is working or applying for graduate students, lay a solid foundation.

The specific plan is as follows. First of all, I want to break this big goal into several sub-goals. Any big task must consist of a series of related sub-tasks. So I break this big goal into three sub-goals for each year, and these three sub-goals are broken down into three small goals for each semester, and so on. In the end, I will put this three-year big. The goal is broken down into a number of small weekly goals.

Moreover, I found that the biggest problem in studying abroad is not that a certain knowledge point is too difficult, but a language problem. Although we have come to study abroad to reach the corresponding language level in advance, it is obviously not enough for a true English learning environment. Especially for English listening, for most English-speaking lecturers, we can't keep up with their rhythm. So I developed a comprehensive English learning plan before I learned the corresponding computer major. Firstly, I will pre-learn each subject before each class, summarize the professional vocabulary that appears, and establish corresponding vocabulary notes. After that, I will review these professional vocabulary regularly. Secondly, learning a language can't just stay in a book. It is also very important to say. So I want to participate in more school clubs, become friends with more locals, and improve my language skills in communication. Thirdly, look at the English-language books and movies, such as Brian Kernighan and Dennis Ritchie's The C Programming Language, which not only helps us learn computers, but also helps us learn English.

For computer science related learning programs, I want to divide the course into lectures, practical and tutorial. These three parts are the main types of courses for our computer science students. If you sort these three parts, it must be the first lecture, practical center and tutorial last. But only these three points are not enough, so I added a preview in front of the lecture, added a check between the practical and the tutorial, and added a review after the tutorial. Why do you do this? The preparation is to better understand the lectures and avoid learning for unnecessary reasons. The check is to find out what parts you do not know or don not clear, and then ask the teacher in the tutorial for these questions. The review is to make the knowledge stronger and make it our own skill.

Furthermore, for computer science related learning programs, we can also divide them by subject. The computer science major is divided into different disciplines. For the first year, we mainly study the six subjects of Application of computer science, Programming, Fundamental of computer science, Software engineering, Probability, and Mathematics for computer science. For me, the mastery of these disciplines is different. Let's talk about mathematics and computer applications. These two subjects are relatively easy for me. I just need to not miss lecture to get them. Probability and applications of computer science are relatively difficult. Each class has knowledge points that I have never touched, and these knowledge points are also difficult. Therefore, my study plan for these subjects is broken one by one. The content should be understood on the same day, do not accumulate problems. For programming, to be honest, it is really difficult. This is a discipline that I have never touched before, and in this subject, there are many programming languages that I don't know. So it takes more effort to learn programming. The first is the programming course, which is attended every section. Second, for the practical of programming, it is best to practice it at home to find the difficult points, which not only saves the practical time, but also improves the pertinence of the problem. Moreover, the most important thing in learning programming is practice. Someone once said: "When you write 10,000 codes, you are afraid that you will not learn programming?" So it is very important to practice more, not only to practice the tasks assigned by the teacher, but also to practice more. Related issues. This will improve your programming skills.

### **Reflections**

In my earlier plan, I have a plan for every day. Then, often cancel or postpone the plan because of various emergencies, which is a bad aspect. Many people have this phenomenon, and the plan will never catch up with change. People are always ambitious when making plans, but after a few days of implementation, they will slowly begin to relax and then give up. How can we reduce or avoid these phenomena? I think we have to start from many aspects. First of all, we must start with the reasons. I often influence the study plan because of the irregular lifestyle. I stay up late and I won’t be able to get up the next day. Or, due to a sudden invitation from a friend, I went to some activities. Furthermore, it affects learning due to illness and so on. What I want to say is that in fact all these reasons can be overcome, regular living, healthy eating and knowing to reject others.

Of course, in the previous plan, there are also good aspects to be done. The group study with classmates is very good. It is really helpful to communicate and learn with English native speakers. Moreover, the advance preparation for the course content is also relatively good, and the identification and annotation of professional vocabulary is not understood. In addition, self-learned professional books outside the course content, such as The C Programming Language. Furthermore, use the playgrounds software to learn programming on the tablet. These are all good aspects that I have done before, and it is worth keeping me going.

### **References**

Wikipedia, (2018, November 13). Computer science. Retrieved from https://en.wikipedia.org/wiki/Computer_science

Programme Specification. Retrieved from http://www.reading.ac.uk/progspecs/pdf18/UFCOMPB18.pdf

![alt text](7bb72a5184b04af7d6f38848220cacd3-0.png "Project Breakdown Structure")

![alt text](2018-11-15 (2).png "CV")

![alt text](3CCA66A1-0BD9-4F0F-8CD0-5790C62D269A.jpeg "Gant")
